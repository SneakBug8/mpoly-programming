# Задачи МосПолитех "Основы Программирования"

- Д/з. 09 - src/tasks15 и src/tasks711
- Птицы
- Git
- Xterm - src/xterm
- Д/з. 10 - src/tasks10
- "1.6 + 1.7 (стр. 192)" - src/tasks1617
- "7.94 + 7.109" - src/tasks794_7109
- Куб - src/cube
- Задачи на строки - src/stringtasks
    - Строки без сплита - src/stringswithoutsplit
- Работа с файлами - src/workwithfiles