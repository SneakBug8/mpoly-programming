package workwithfiles;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

// word count with 3 r's

public class task2 {
	static String path;
	static File file;
	static BufferedReader br;
	static Scanner scr = new Scanner(System.in);
	static int count = 0;

	public static void main(String[] args) {
		while (true) {
			System.out.println("Insert file path");
			path = Input();
			file = new File(path);
			if (file.exists()) {
				if (file.canRead())
					break;
				else
					System.out.println("Cannot read that file");
			} else
				System.out.println("No file exists");
		}

		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

			String line = null;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] words = line.split(" ");
				int rcount = 0;
				for (int i = 0; i < words.length; i++) {
					rcount = 0;
					for (int j = 0; j < words[i].length(); j++) {
						if (words[i].charAt(j) == 'r')
							rcount++;
					}
					if (rcount >= 3) {
						count++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Number of words with at least 3 r's 'r' = " + count);

	}

	private static String Input() {
		String str;

		while (true) {
			try {

				str = scr.nextLine();
				break;
			} catch (Exception e) {
				System.out.println(e + " - Something went wrong. Try again");
				scr.next();
			}
		}
		return str;
	}

}
