package workwithfiles;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

// r symb count in file (absolute path)

public class task1 {
	static String path;
	static File file;
	static BufferedReader br;
	static Scanner scr = new Scanner(System.in);
	static int count = 0;

	public static void main(String[] args) {
		while (true) {
			System.out.println("Insert file path");
			path = Input();
			file = new File(path);
			if (file.exists()) {
				if (file.canRead())
					break;
				else
					System.out.println("Unable to read file");
			} else
				System.out.println("Such file doesn't exist");
		}

		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

			String line = null;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				for (int i = 0; i < line.length(); i++) {
					if (line.charAt(i) == 'r')
						count++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Number of 'r' symbols: " + count);

	}

	private static String Input() {
		String str;

		while (true) {
			try {

				str = scr.nextLine();
				break;
			} catch (Exception e) {
				System.out.println(e + " - Something went wrong. Try again");
				scr.next();
			}
		}
		return str;
	}

}
