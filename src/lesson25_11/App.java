package lesson25_11;

class App {
    public static void main (String[] args) {
        int[] a = {-1, -1, -1, -1, -1};

        for (int i = 1; i < a.length - 1;) {
            a[i] = ++i + 10;
        }

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + "_");
        }

        System.out.print("\n");
    }
}