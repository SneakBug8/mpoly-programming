package tasks1617;

public class Vector3 {
    public double x;
    public double y;
    public double z;

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3 TranslateX(double n) {
        this.x += n;
        return this;
    }

    public Vector3 TranslateY(double n) {
        this.y += n;
        return this;
    }

    public Vector3 TranslateZ(double n) {
        this.z += n;
        return this;
    }

    public Vector3 Translate(double x, double y, double z) {
        return this.TranslateX(x).TranslateY(y).TranslateZ(z);
    }

    public void Print() {
        System.out.printf("X: %.2f; Y: %.2f; Z: %.2f;\n", this.x, this.y, this.z);
    }

    public double Length() {
        return Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
    }

    public Vector3 RotateX(double n) {
        n = Math.toRadians(n);
        var x1 = x;
        var y1 = y * Math.cos(n) + z * Math.sin(n);
        var z1 = -y * Math.sin(n) + z * Math.cos(n);
        
        this.x = x1;
        this.y = y1;
        this.z = z1;
        return this;
    }

    public Vector3 RotateY(double n) {
        n = Math.toRadians(n);
        var x1 = x * Math.cos(n) + z * Math.sin(n);
        var y1 = y;
        var z1 = -x * Math.sin(n) + z * Math.cos(n);

        this.x = x1;
        this.y = y1;
        this.z = z1;
        return this;
    }

    public Vector3 RotateZ(double n) {
        n = Math.toRadians(n);
        var x1 = x * Math.cos(n) - y * Math.sin(n);
        var y1 = -x * Math.sin(n) + y * Math.cos(n);
        var z1 = z;

        this.x = x1;
        this.y = y1;
        this.z = z1;
        return this;
    }

    public Vector3 Rotate(double x, double y, double z) {
        return this.RotateX(x).RotateY(y).RotateZ(z);
    }

    public Vector3 Scale(double x, double y, double z) {
        this.x *= x;
        this.y *= y;
        this.z *= z;
        return this;
    }

    public Vector3 MultiplyBy(double n) {
        this.x *= n;
        this.y *= n;
        this.z *= n;
        return this;
    }

    public Vector3 Add(Vector3 v) {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
        return this;
    }

    public Vector3 Substract(Vector3 v) {
        return this.Add(v.MultiplyBy(-1));
    }

    public double MultiplyScalar(Vector3 v) {
        return this.x * v.x + this.y * v.y;
    }

    public Vector3 MultiplyVector(Vector3 v) {
        return new Vector3((this.y * v.z - v.y * this.z), (this.x * v.z - v.x * this.z), (this.x * v.y - v.x * this.y));
    }

    public Vector3 Clone() {
        return new Vector3(x,y,z);
    }
}