package tasks711;

import java.util.InputMismatchException;
import java.util.Scanner;

class Task7 {
    protected static Scanner scanner;
    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        int a = GetIntFromConsole();
        int b = GetIntFromConsole();

        a = a + b;
        b = a - b;
        a -= b;

        System.out.println("a - " + a);
        System.out.println("b - " + b);
    }

    static int GetIntFromConsole() {
        int temp;

        try {
            temp = scanner.nextInt();
        }
        catch (InputMismatchException e) {
            throw e;
        }

        return temp;
    }
}