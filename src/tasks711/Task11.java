package tasks711;

import java.util.InputMismatchException;
import java.util.Scanner;

class Task11 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        int a = GetIntFromConsole();
        int b = GetIntFromConsole();
        int c = GetIntFromConsole();


        if (b * b - 4 * a * c < 0) {
            throw new Error("Отрицательный дискриминант");
        }

        double d = Math.sqrt(b * b - 4 * a * c);
        double x1 = (-b + d) / (2 * a);
        double x2 = (-b - d) / (2 * a);

        System.out.println("Первый корень - " + x1);
        System.out.println("Вторый корень - " + x2);

    }

    static int GetIntFromConsole() {
        int temp;

        try {
            temp = scanner.nextInt();
        }
        catch (InputMismatchException e) {
            throw e;
        }

        return temp;
    }
}