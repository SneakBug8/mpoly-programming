package stringtasks;

import java.util.Arrays;
import java.util.Scanner;

class Task2 {
    public static void main(String[] args) throws Exception {
        var scanner = new Scanner(System.in, "utf-8");

        var sentence = scanner.nextLine();

        var words = sentence.split("\\s+");

        int i = 0;
        for (var word : words) {
            if (word.length() == 3) {
                i++;
            }
        }

        System.out.println("String contains words with length 3 - " + i);
    }
}