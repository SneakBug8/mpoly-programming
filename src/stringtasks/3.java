package stringtasks;

import java.util.Scanner;

class Task3 {
    public static void main(String[] args) throws Exception {
        var scanner = new Scanner(System.in, "utf-8");

        var sentence = scanner.nextLine();

        var words = sentence.split("\\s+");

        int i = 0;
        String longestword = "";
        for (var word : words) {
            if (word.length() >= i) {
                i = word.length();
                longestword = word;
            }
        }

        System.out.println("Most lenght - " + i);
        System.out.println("The longest word - " + longestword);
    }
}