package stringtasks;

import java.util.Arrays;
import java.util.Scanner;

class Task1 {
    public static void main(String[] args) throws Exception {
        var scanner = new Scanner(System.in, "utf-8");

        var sentence = scanner.nextLine();

        var words = sentence.split("\\s+");
        var vowels = Arrays.asList(new String[] { "a", "e", "y", "u", "i", "o", });

        int i = 0;
        for (var word : words) {
            var character = word.charAt(0);

            System.out.println(character);

            if (vowels.contains(Character.toString(character))) {
                i++;
            }
        }

        System.out.println("String contains vovels " + i);
    }
}