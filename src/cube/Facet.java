package cube;

import tasks1617.Vector3;

import java.awt.*;
import java.awt.geom.*;

class Facet {
    public Vector3[] vertex;
    public Color Color;

    // private Color Color;
    public Facet(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4) {
        vertex = new Vector3[] { v1, v2, v3, v4 };

        Color = Color.BLACK;
    }

    public Vector3 v1() {
        return vertex[0];
    }

    public Vector3 v2() {
        return vertex[1];
    }

    public Vector3 v3() {
        return vertex[2];
    }

    public Vector3 v4() {
        return vertex[3];
    }

    public void out() {
        System.out.printf("(%.2f;%.2f;%.2f) - (%.2f;%.2f;%.2f) \n", v1().x, v1().y, v1().z, v2().x, v2().y, v2().z);
        System.out.printf("(%.2f;%.2f;%.2f) - (%.2f;%.2f;%.2f) \n", v2().x, v2().y, v2().z, v3().x, v3().y, v3().z);
        System.out.printf("(%.2f;%.2f;%.2f) - (%.2f;%.2f;%.2f) \n", v3().x, v3().y, v3().z, v4().x, v4().y, v4().z);
        System.out.printf("(%.2f;%.2f;%.2f) - (%.2f;%.2f;%.2f) \n", v4().x, v4().y, v4().z, v1().x, v1().y, v1().z);
    }

    public void Rotate(double x, double y, double z) {
        for (var point : this.vertex) {
            point.Rotate(x, y, z);
        }
    }

    public void Translate(double x, double y, double z) {
        for (var point : this.vertex) {
            point.Translate(x, y, z);
        }
    }

    public void Multiply(double x, double y, double z) {
        for (var point : this.vertex) {
            point.Scale(x, y, z);
        }
    }

    public Vector3 Normal() {
        var first = v2().Clone().Substract(v1().Clone());
        var second = v4().Clone().Substract(v1().Clone());

        return first.MultiplyVector(second).Add(v1());
    }

    public double Space() {
        var a = SpaceBetweenTwoPoints(v1(), v2());
        var b = SpaceBetweenTwoPoints(v2(), v3());
        var c = SpaceBetweenTwoPoints(v3(), v1());
        var p = ( a + b + c) / 2;
        var s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return s;
    }

    public static double SpaceBetweenTwoPoints(Vector3 x1, Vector3 x2) {
        return Math.sqrt(Math.pow(x1.x - x2.x, 2) + Math.pow(x1.y - x2.y, 2) + Math.pow(x1.z - x2.z, 2));
    }

    public void draw(Graphics2D g) {
        g.setColor(Color);

        Path2D p = new Path2D.Double();
        p.moveTo(v1().x, v1().y);
        p.lineTo(v2().x, v2().y);
        p.lineTo(v3().x, v3().y);
        p.lineTo(v4().x, v4().y);
        p.lineTo(v1().x, v1().y);
        p.closePath();
        if (this.Normal().z < 0) {
            g.fill(p);
        }

        g.setColor(Color.BLACK);
    }
}