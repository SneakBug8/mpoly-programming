package cube;

import java.awt.*;

import javax.swing.*;

import tasks1617.Vector3;

public class CubeWindow extends JFrame {
    private Cube Cube;

    public CubeWindow(Cube cube) {
        super("CubeWindow");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.setVisible(true);

        this.Cube = cube;
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(new Color(0, 0, 0));

        for (var facet : Cube.facets) {
            facet.draw((Graphics2D) g);
        }
    }
}