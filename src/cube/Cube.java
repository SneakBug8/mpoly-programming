package cube;

import java.awt.Color;

import tasks1617.Vector3;

class Cube {
    public Facet[] facets;

    public Cube(Facet f1, Facet f2, Facet f3, Facet f4, Facet f5, Facet f6) {
        this.facets = new Facet[] { f1, f2, f3, f4, f5, f6 };
    }

    public static void main(String[] args) {

        var point1 = new Vector3(0, 0, 0);
        var point2 = new Vector3(0, 0, 1);
        var point3 = new Vector3(0, 1, 0);
        var point4 = new Vector3(1, 0, 0);
        var point5 = new Vector3(1, 1, 0);
        var point6 = new Vector3(0, 1, 1);
        var point7 = new Vector3(1, 0, 1);
        var point8 = new Vector3(1, 1, 1);

        var facet1 = new Facet(new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(1, 1, 0), new Vector3(0, 1, 0));
        facet1.Color = Color.RED;
        var facet2 = new Facet(new Vector3(1, 0, 1), new Vector3(1, 1, 1), new Vector3(1, 1, 0), new Vector3(1, 0, 0));
        facet2.Color = Color.GREEN;

        var facet3 = new Facet(new Vector3(0, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 1, 1), new Vector3(0, 0, 1));
        facet3.Color = Color.YELLOW;

        var facet4 = new Facet(new Vector3(0, 0, 0), new Vector3(0, 0, 1), new Vector3(1, 0, 1), new Vector3(1, 0, 0));

        facet4.Color = Color.blue;

        var facet5 = new Facet(new Vector3(0, 1, 0), new Vector3(1, 1, 0), new Vector3(1, 1, 1), new Vector3(0, 1, 1));

        facet5.Color = Color.PINK;

        var facet6 = new Facet(new Vector3(0, 0, 1), new Vector3(0, 1, 1), new Vector3(1, 1, 1), new Vector3(1, 0, 1));

        facet6.Color = Color.gray;


        var cube = new Cube(facet1, facet2, facet3, facet4, facet5, facet6);

        cube.Multiply(100, 100, 100);
        cube.Translate(500, 500, 0);
        cube.Space();
        cube.Rotate(60, 60, 0);
        cube.Space();
        cube.Rotate(60, 60, 0);
        cube.Space();
        cube.Rotate(60, 60, 0);
        cube.Space();

        var cuberenderer = new CubeWindow(cube);
    }

    public void Rotate(double x, double y, double z) {
        for (var facet : this.facets) {
            facet.Rotate(x, y, z);
        }
    }

    public void Space() {
        System.out.println("Facets are drawing");
        System.out.println(facets[0].Space());
        System.out.println(facets[1].Space());
        System.out.println(facets[2].Space());
        System.out.println(facets[3].Space());
        System.out.println(facets[4].Space());
        System.out.println(facets[5].Space());
        System.out.println("Facets were drawn");
    }

    public void Translate(double x, double y, double z) {
        for (var point : this.facets) {
            point.Translate(x, y, z);
        }
    }

    public void Multiply(double x, double y, double z) {
        for (var point : this.facets) {
            point.Multiply(x, y, z);
        }
    }
}