package tasks794_7109;

import java.util.Scanner;

class NOD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Integer i = scanner.nextInt();
        Integer j = scanner.nextInt();

        System.out.println(NOD(i, j));
    }

    public static Integer NOD(Integer i, Integer j) {
        int module = i % j;

        if (module == 0) {
            return j;
        } else {
            return NOD(i, module);
        }
    }
}