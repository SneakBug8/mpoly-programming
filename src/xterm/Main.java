package xterm;

public class Main {
    public static void main(String[] args) throws Exception {
        double first = Xterm.inputInt();
        double second = Xterm.inputInt();
        double third = Xterm.inputInt();

        double medium = (first + second + third) / 3;

        Xterm.println("First number: " + first, Xterm.ANSI_RED);
        Xterm.println("Second number: " + second, Xterm.ANSI_GREEN);
        Xterm.println("Third number: " + third, Xterm.ANSI_BLUE);

        Xterm.println("Medium: " + medium, Xterm.ANSI_CYAN);
    }
}