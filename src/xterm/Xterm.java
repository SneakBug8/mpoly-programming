package xterm;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Scanner;

// Класс, обеспечивающий вывод строк текста с возможностью
// позиционирования и использования цветов, а также ввод чисел
// целых типов int и long и вещественных float и double.
public class Xterm {

    private static final DataInputStream in = new DataInputStream(System.in);
    private static final int MAXLEN = 255;

    private static String inputString() throws IOException {
        byte buf[] = new byte[MAXLEN];
        int i = in.read(buf);
        return new String(buf, 0, i - 1);
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    // Метод очистки экрана
    public static void clear() {
        System.out.print("\033[2J");
    }

    // Метод позиционирования курсора
    public static void setPosition(int x, int y) {
        System.out.print("\033[" + (y + 1) + ";" + (x + 1) + "H");
    }

    // Методы вывода строки
    public static void print(String txt) {
        System.out.print(txt);
    }

    public static void print(String txt, String color) {
        System.out.print(color + txt + ANSI_WHITE);
    }

    public static void println(String txt) {
        print(txt + "\n");
    }

    public static void println(String txt, String fg) {
        print(txt + "\n", fg);
    }

    // Методы ввода чисел типов int, long, float, double
    public static int inputInt() throws IOException, NumberFormatException {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            int num = scanner.nextInt();
            return num;
        }

        scanner.close();

        return 0;
    }

    public static int inputInt(String prompt) throws IOException, NumberFormatException {
        print(prompt);
        return inputInt();
    }

    public static long inputLong() throws IOException, NumberFormatException {
        return Long.valueOf(inputString()).longValue();
    }

    public static long inputLong(String prompt) throws IOException, NumberFormatException {
        print(prompt);
        return inputLong();
    }

    public static float inputFloat() throws IOException, NumberFormatException {
        return Float.valueOf(inputString()).floatValue();
    }

    public static float inputFloat(String prompt) throws IOException, NumberFormatException {
        print(prompt);
        return inputFloat();
    }

    public static double inputDouble() throws IOException, NumberFormatException {
        return Double.valueOf(inputString()).doubleValue();
    }

    public static double inputDouble(String prompt) throws IOException, NumberFormatException {
        print(prompt);
        return inputDouble();
    }

    // Методы ввода строки, рассматриваемой как массив символов.
    public static char[] inputChars() throws IOException {
        return (inputString()).toCharArray();
    }

    public static char[] inputChars(String prompt) throws IOException {
        print(prompt);
        return (inputString()).toCharArray();
    }
}