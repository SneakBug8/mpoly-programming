package tasks10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Dz10 {
    static Scanner in;

    public static void main(String[] args) {
        in = new Scanner(System.in);

        switch (in.nextInt()) {
        case 133:
            t133();
            break;
        case 134:
            t134(true);
            break;
        case 135:
            t135();
            break;
        case 136:
            t136();
            break;
        case 137:
            t137();
            break;
        case 138:
            t138();
            break;
        case 139:
            t139();
            break;
        case 140:
            t140();
            break;
        case 141:
            t141();
            break;
        case 142:
            t142();
            break;
        case 143:
            t143();
            break;
        case 144:
            t144();
            break;
        case 145:
            t145();
            break;
        }
    }

    public static void t133() {
        int a, max = Integer.MIN_VALUE;
        int count = 0;
        

        while (true) {
            a = in.nextInt();

            if (a == 0) {
                break;
            }

            max = Math.max(a, max);
            if (a != max && max == a) {
                count++;
            } else {
                count = 0;
            }
        }
        System.out.println(count);
    }

    public static void t134(boolean rand) {
        HashSet<Integer> set = new HashSet<Integer>();
        int a;

        if (rand) {
            System.out.println("-- rand --");

            int n = (int) (Math.random() * Integer.MAX_VALUE / 2);
            for (int i = 0; i < n; i++) {
                set.add((int) (Math.random() * Integer.MAX_VALUE / 2));
            }

            System.out.println("-- rand --");
        } else {
            while (in.hasNextInt()) {
                a = in.nextInt();
                set.add(Math.abs(a));
            }
        }

        System.out.println(set.size());
    }

    public static void t135() {
        double a = 0;
        double sum = 0;
        double count = 0;

        while (in.hasNextDouble()) {
            a = in.nextDouble();
            sum += a;
            count++;
        }

        System.out.println(sum / count);
    }

    public static void t136() {
        int a = 0;
        int b = in.nextInt();
        ;
        int count = 0;
        int maxCount = 0;

        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a == b)
                count++;
            else {
                if (count > maxCount) {
                    maxCount = count;
                    count = 0;
                }
            }
            b = a;
        }
        if (count > maxCount) {
            maxCount = count;
        }

        System.out.println(maxCount);
    }

    public static void t137() {
        int a, max = Integer.MIN_VALUE;
        int indexA = 0, indexF = 0, indexL = 0;

        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a > max) {
                max = a;
                indexL = indexF;
                indexF = indexA;
            }
            indexA++;
        }

        System.out.println(indexF + " " + indexL);
    }

    public static void t138() {
        int a, index = 0;
        boolean hasNull = false;
        while (in.hasNextInt()) {
            a = in.nextInt();

            hasNull = (a == 0) ? true : false;
            if (hasNull) {
                index++;
                break;
            }
        }

        if (hasNull) {
            System.out.println(index);
        } else {
            System.out.println("0!");
        }
    }

    public static void t139() {
        int a, b, count = 1;
        b = in.nextInt();
        while (in.hasNextInt()) {
            a = in.nextInt();

            if (a > b) {
                count++;
            }

            b = a;
        }
        System.out.println(count);
    }

    public static void t140() {
        int a, maxF = Integer.MIN_VALUE;
        int maxS = Integer.MIN_VALUE;

        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a > maxF) {
                maxS = maxF;
                maxF = a;
            } else if (a > maxS) {
                maxS = a;
            }
        }

        if (maxS == Integer.MIN_VALUE) {
        } else {
            System.out.println(maxS);
        }
    }

    public static void t141() {
        int a;
        ArrayList<Integer> num = new ArrayList<Integer>();
        while (in.hasNextInt()) {
            a = in.nextInt();
            num.add(a);
        }

        for (int i = 0; i < num.size(); i++) {
            for (int j = 0; j < num.size(); j++) {
                for (int k = 0; k < num.size(); k++) {
                    if (num.get(i) * num.get(j) == num.get(k)) {
                        System.out.println(num.get(i) + " " + num.get(j) + " " + num.get(k));
                        return;
                    }
                }
            }
        }
        System.out.println("No");
    }

    public static void t142() {
        int a, b, up = 1, upMax = 1, down = 1, downMax = 1;
        b = in.nextInt();
        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a > b)
                up++;
            else {
                if (up > upMax)
                    upMax = up;
                up = 1;
            }
            if (a < b)
                down++;
            else {
                if (down > downMax)
                    downMax = down;
                down = 1;
            }
            b = a;
        }
        if (up > upMax)
            upMax = up;
        if (down > downMax)
            downMax = down;
        if (upMax > downMax)
            System.out.println(upMax);
        else
            System.out.println(downMax);
    }

    public static void t143() {
        int a, b, count = 0;
        boolean begin = false, end = false;
        b = in.nextInt();
        while (in.hasNextInt()) {
            a = in.nextInt();
            if (b == 1 && a == 0 && !end)
                begin = true;
            if (b == 0 && a == 1 && begin)
                end = true;
            if (begin && end) {
                count++;
                begin = false;
                end = false;
            }
            b = a;
        }
        System.out.println(count);
    }

    public static void t144() {
        int a, count = 0, i = 0;
        int[] frag = { 1, 2, 3, 4, 5, 6 };

        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a == frag[i])
                i++;
            else {
                i = 0;
                if (a == frag[0])
                    i++;
            }
            if (i == frag.length) {
                i = 0;
                count++;
            }
        }
        System.out.println(count);
    }

    public static void t145() {
        int a, count = 0, i = 0;
        int[] frag = { 1, 2, 1, 2, 1, 2 };
        while (in.hasNextInt()) {
            a = in.nextInt();
            if (a == frag[i])
                i++;
            else {
                i = 0;
                if (a == frag[0])
                    i++;
            }
            if (i == frag.length - 1) {
                i = 0;
                count++;
            }
        }
        System.out.println(count);
    }
}
